package sample;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Andrei on 18/04/2016.
 */
public class Car {
    private SimpleLongProperty car_id;
    private SimpleStringProperty brand;
    private SimpleStringProperty model;
    private SimpleStringProperty fuel_type;
    private SimpleStringProperty registration_number;
    private SimpleStringProperty first_reg_year_month;
    private SimpleLongProperty km;
    private SimpleStringProperty car_type;

    public Car(long car_id,String brand,String model,String fuel_type,
               String registration_number,String first_reg_year_month,long km,String car_type){

        this.car_id = new SimpleLongProperty(car_id);
        this.brand = new SimpleStringProperty(brand);
        this.model = new SimpleStringProperty(model);
        this.fuel_type = new SimpleStringProperty(fuel_type);
        this.registration_number = new SimpleStringProperty(registration_number);
        this.first_reg_year_month = new SimpleStringProperty(first_reg_year_month);
        this.km = new SimpleLongProperty(km);
        this.car_type = new SimpleStringProperty(car_type);

    }

    public long getCar_id() {
        return car_id.get();
    }

    public SimpleLongProperty car_idProperty() {
        return car_id;
    }

    public void setCar_id(long car_id) {
        this.car_id.set(car_id);
    }

    public String getBrand() {
        return brand.get();
    }

    public SimpleStringProperty brandProperty() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand.set(brand);
    }

    public String getModel() {
        return model.get();
    }

    public SimpleStringProperty modelProperty() {
        return model;
    }

    public void setModel(String model) {
        this.model.set(model);
    }

    public String getFuel_type() {
        return fuel_type.get();
    }

    public SimpleStringProperty fuel_typeProperty() {
        return fuel_type;
    }

    public void setFuel_type(String fuel_type) {
        this.fuel_type.set(fuel_type);
    }

    public String getRegistration_number() {
        return registration_number.get();
    }

    public SimpleStringProperty registration_numberProperty() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number.set(registration_number);
    }

    public String getFirst_reg_year_month() {
        return first_reg_year_month.get();
    }

    public SimpleStringProperty first_reg_year_monthProperty() {
        return first_reg_year_month;
    }

    public void setFirst_reg_year_month(String first_reg_year_month) {
        this.first_reg_year_month.set(first_reg_year_month);
    }

    public String getCar_type() {
        return car_type.get();
    }

    public SimpleStringProperty car_typeProperty() {
        return car_type;
    }

    public void setCar_type(String car_type) {
        this.car_type.set(car_type);
    }

    public long getKm() {
        return km.get();
    }

    public SimpleLongProperty kmProperty() {
        return km;
    }

    public void setKm(long km) {
        this.km.set(km);
    }
}
