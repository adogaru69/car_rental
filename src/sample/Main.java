package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

import java.sql.*;
import java.util.*;
import java.io.*;

public class Main extends Application {

    static final String DB_URL = "jdbc:mysql://localhost/car_rental";
    static final String USER = "root";
    static final String PASS = "mysql";

    Stage window;
    Scene scene;
    ObservableList<Car> cars;
    TableView<Car> table;

    @Override
    public void start(Stage primaryStage) throws Exception{
        window = primaryStage;

        cars = FXCollections.observableArrayList();
        getCarsFromDatabase();

        //          ****************** CREATE TABLE ******************
        table = new TableView<>();
        table.itemsProperty().setValue(cars);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        TableColumn<Car, Number> carIdColumn = new TableColumn<>("car_id");
        TableColumn<Car, String> brandColumn = new TableColumn<>("brand");
        TableColumn<Car, String> modelColumn = new TableColumn<>("model");
        TableColumn<Car, String> fuelTypeColumn = new TableColumn<>("fuel_type");
        TableColumn<Car, String> regNumColumn = new TableColumn<>("registration_number");
        TableColumn<Car, String> firstRegYMColumn = new TableColumn<>("first_reg_year_month");
        TableColumn<Car, Number> kmColumn = new TableColumn<>("km");
        TableColumn<Car, String> typeColumn = new TableColumn<>("car_type");
        table.getColumns().addAll(carIdColumn,brandColumn,modelColumn,fuelTypeColumn,regNumColumn,
                                  firstRegYMColumn,kmColumn,typeColumn);

        //Attach Action Listeners
        carIdColumn.setCellValueFactory(e -> e.getValue().car_idProperty());
        brandColumn.setCellValueFactory(e -> e.getValue().brandProperty());
        modelColumn.setCellValueFactory(e -> e.getValue().modelProperty());
        fuelTypeColumn.setCellValueFactory(e -> e.getValue().fuel_typeProperty());
        regNumColumn.setCellValueFactory(e -> e.getValue().registration_numberProperty());
        firstRegYMColumn.setCellValueFactory(e -> e.getValue().first_reg_year_monthProperty());
        kmColumn.setCellValueFactory(e -> e.getValue().kmProperty());
        typeColumn.setCellValueFactory(e -> e.getValue().car_typeProperty());

        carIdColumn.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
        brandColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        modelColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        fuelTypeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        regNumColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        firstRegYMColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        kmColumn.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
        typeColumn.setCellFactory(TextFieldTableCell.forTableColumn());


        BorderPane root = new BorderPane();
        root.setCenter(table);
        scene = new Scene(root, 800, 300);

        window.setTitle("Hello World");
        window.setScene(scene);
        window.show();
    }

    public void getCarsFromDatabase(){
        Connection conn = null;
        Statement stmt = null;
        try{

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String sql = "select * from cars";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()){

                long car_id = rs.getLong("car_id");
                String brand = rs.getString("brand");
                String model = rs.getString("model");
                String fuel_type = rs.getString("fuel_type");
                String registration_number = rs.getString("registration_number");
                String first_reg_year_month = rs.getString("first_reg_year_month");
                long km = rs.getLong("km");
                String car_type = rs.getString("car_type");

                Car car = new Car(car_id,brand,model,fuel_type,
                        registration_number,first_reg_year_month,km,car_type);

                cars.add(car);
            }

        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}
